﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WPExanteListenerOrders.Entities
{
    public class ProcessingTimeOutEvent
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Status { get; set; }

        public string OrderTag { get; set; }

        public string OrderId { get; set; }

        public DateTime LastUpdatedUTCTime { get; set; }
    }

}
