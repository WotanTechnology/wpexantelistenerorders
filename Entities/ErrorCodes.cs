﻿using System;
namespace WPExanteListenerOrders.Entities
{
    public static class ErrorCodes
    {
        public const int InvalidUserPassword = 1001;
        public const int InvalidUDID= 1002;
        public const int LoginFailed = 1003;
        public const int InvalidVerificationCode = 1004;
        public const int PhoneNumberNotValid = 1005;
        public const int EmailAlreadyTaken = 1006;
        public const int PhoneNumberAlreadyTaken = 1007;
        public const int UserNotFound = 1008;
        public const int INNNotValid = 1009;
        public const int TradeDocumentInvalid = 1010;
        public const int InvalidEmail = 1011;
        public const int InvalidAccessToken = 1012;
        public const int OnSign = 1013;
        public const int UserHasBeenAlreadyVerified = 1014;
        public const int NotEnoughMoney = 1015;
        public const int ItemDataNotFound = 1016;
        public const int NotEnoughCount = 1017;
        public const int TradeTimeOut = 1018;
        public const int WaitingForExecution = 1019;
        public const int OutOfWorkingHours = 1020;
        public const int InvalidToken = 1021;
        public const int TokenHasNotExpiryYet = 1022;
        public const int TokenDoesNotExist = 1023;
        public const int RefreshTokenHasExpired = 1024;
        public const int RefreshTokenHasBeenInvalidated = 1025;

        public const int RefreshTokenHasBeenUsed = 1026;

        public const int RefreshTokenDoesNotExist = 1027;
        public const int BlockOneDayTradeSameIsin = 1028;

        public const int EmptyRequisites = 1029;

        public const int InvalidMultiple = 1030;

        public const int RemoteAccountOpeningConnectionError = 1031;

        public const int InvalidSummOfPercent = 1032;
        public const int InvalidConfig = 1033;
        public const int InvalidDataFromBroker = 1034;
        public const int NotEnoughPrice = 1035;
    }


    public struct Error
    {
        public int code;
        public string message;
    }
}
