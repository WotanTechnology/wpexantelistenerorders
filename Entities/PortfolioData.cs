﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WPExanteListenerOrders.Entities
{
    public class PortfolioData
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string UserId { get; set; }
        public string Email { get; set; }

        public DateTime UpdatedTime { get; set; }

        public string BrokerAccountId { get; set; }

        public string BrokerName { get; set; }

        public decimal Summ { get; set; }

        public string Strategy { get; set; }

        public bool Last { get; set; }

        public Position[] TradePosition { get; set; }

        public OrderOperation[] OrderOperations { get; set; }
    }

    public class Position
    {
        public string SymbolId { get; set; }
        public int Quantity { get; set; }
        public decimal Percent { get; set; }
    }

    public class OrderOperation
    {
        public string SymbolId { get; set; }
        public int Quantity { get; set; }
        public decimal AveragePrice { get; set; }
        public string OperationType { get; set; }
        public string Tag { get; set; }
        public string Status { get; set; }
    }
}
