﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace WPExanteListenerOrders.Entities
{
    public class RequestExanteOrder
    {
        public string side { get; set; }
        public string quantity { get; set; }
        public string duration { get; set; }
        public string orderType { get; set; }
        public string clientTag { get; set; }
        public string accountId { get; set; }
        public string symbolId { get; set; }
        public string instrument { get; set; }
        public string limitPrice { get; set; }

        public string gttExpiration { get; set; }
        public string ifDoneParentId { get; set; }
        public string takeProfit { get; set; }
        public string placeInterval { get; set; }
        public string ocoGroup { get; set; }
        public string stopPrice { get; set; }
        public string stopLoss { get; set; }
        public string partQuantity { get; set; }
        public string priceDistance { get; set; }
    }

    public class ExanteOrderSide
    {
        public const string buy = "buy";
        public const string sell = "sell";
    }

    public class ExanteOrderDuration
    {
        public const string day = "day";
        public const string fill_or_kill = "fill_or_kill";
        public const string immediate_or_cancel = "immediate_or_cancel";
        public const string good_till_cancel = "good_till_cancel";
        public const string good_till_time = "good_till_time";
        public const string at_the_opening = "at_the_opening";
        public const string at_the_close = "at_the_close";
    }

    public class ExanteOrderType
    {
        public const string market = "market";
        public const string limit = "limit";
        public const string stop = "stop";
        public const string stop_limit = "stop_limit";
        public const string twap = "twap";
        public const string trailing_stop = "trailing_stop";
        public const string iceberg = "iceberg";
    }

    public class ResponceExanteOrder
    {
        [BsonId]
        public string id { get; set; }

        public OrderParameters orderParameters { get; set; }
        public OrderState orderState { get; set; }
        public string clientTag { get; set; }
        public string currentModificationId { get; set; }
        public string username { get; set; }
        public string accountId { get; set; }
        public string orderId { get; set; }
        public DateTime placeTime { get; set; }
        public BrokerInfo broker { get; set; }
        public string strategyName { get; set; }
    }

    public class OrderParameters
    {
        public string side { get; set; }
        public DateTime gttExpiration { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ifDoneParentId { get; set; }
        public string partQuantity { get; set; }
        public string placeInterval { get; set; }
        public decimal limitPrice { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ocoGroup { get; set; }
        public string orderType { get; set; }
        public decimal stopPrice { get; set; }

        public int quantity { get; set; }
        public string duration { get; set; }
        public string instrument { get; set; }
        public string symbolId { get; set; }
        public decimal priceDistance { get; set; }
    }

    public class OrderState
    {
        public DateTime lastUpdate { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
        public Fills[] fills { get; set; }
    }

    public class Fills
    {
        public DateTime timestamp { get; set; }
        public decimal price { get; set; }
        public decimal position { get; set; }
        public DateTime time { get; set; }
        public string quantity { get; set; }
    }

    public static class ExanteOrderStatus
    {
        public const string Placing = "placing";
        public const string Filled = "filled";
        public const string Working = "working";
        public const string Pending = "pending";
        public const string Rejected = "rejected";
        public const string Canceled = "cancelled";
        public const string BadRequest = "bad_request";
    }

}
