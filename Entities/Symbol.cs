﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WPExanteListenerOrders.Entities
{
    public class Symbol
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string SymbolId { get; set; }
    }

    public class TargetPriceModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string SymbolId { get; set; }
        public string Side { get; set; }
        public decimal Price { get; set; }
        public string Status { get; set; }
    }
}
