﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WPExanteListenerOrders.Entities
{
    public class AchievementTargetPriceEvent
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Strategy { get; set; }

        public string Broker { get; set; }

        public string SymbolId { get; set; }

        public decimal CurrentPrice { get; set; }

        public string Status { get; set; }
    }

    public static class EventStatus
    {
        public const string Placed = "placed";
        public const string Processed = "processed";
    }
}
