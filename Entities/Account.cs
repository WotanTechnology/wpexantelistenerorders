﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace WPExanteListenerOrders.Entities
{
    public class Account
    {
        public string accountId { get; set; }
        public string status { get; set; }
    }

    public class AccountSummary
    {
        public TradePosition[] positions { get; set; }
        public string currency { get; set; }
        public string marginUtilization { get; set; }
        public Currency[] currencies { get; set; }
        public string account { get; set; }
        public string accountId { get; set; }
        public decimal freeMoney { get; set; }
        public decimal netAssetValue { get; set; }
        public decimal moneyUsedForMargin { get; set; }
        public ulong timestamp { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime sessionDate { get; set; }
    }

    public class Currency
    {
        public decimal convertedValue { get; set; }
        public string code { get; set; }
        public decimal value { get; set; }
    }

    public class TradePosition
    {
        public string symbolId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal averagePrice { get; set; }

        public string currency { get; set; }
        public int quantity { get; set; }
        public decimal value { get; set; }
        public decimal price { get; set; }
        public string id { get; set; }
        public decimal convertedValue { get; set; }
        public decimal convertedPnl { get; set; }
        public decimal pnl { get; set; }
        public string symbolType { get; set; }
    }

    public class PriceInfo
    {
        public decimal price { get; set; }
        public decimal size { get; set; }
        public decimal value { get; set; }
    }

    public class QuoteInfo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }


        public string symbolId { get; set; }
        public ulong timestamp { get; set; }
        public PriceInfo[] ask { get; set; }
        public PriceInfo[] bid { get; set; }
    }
}
