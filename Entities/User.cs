﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WPExanteListenerOrders.Entities
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Role { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Telegram { get; set; }

        public DateTime BirthDay { get; set; }
        public decimal AverageMonthlyExpenses { get; set; }
        public decimal AverageMonthlyIncome { get; set; }
        public decimal CumulativeTarget { get; set; }

        public AddressData Address { get; set; }

        public RiskProfileEnum RiskProfileType { get; set; }
        public BrokerInfo[] BrokerInfos { get; set; }


    }

    public class AddressData
    {
        public string Country { get; set; }
        public string District { get; set; }
        public string City { get; set; }
    }


    public class BrokerInfo
    {
        public string BrokerName { get; set; }
        public string UserName { get; set; }
        public string ClientId { get; set; }
        public string ApplicationID { get; set; }
        public string SharedKey { get; set; }
        public string Environment { get; set; }
        public string AccessKey { get; set; }
        public string AccountId { get; set; }
    }

    public enum RiskProfileEnum
    {
        very_risky = 0,
        risky = 1,
        balanced = 2,
        conservative = 3
    }

    public static class Broker
    {
        public const string Exante = "Exante";
        public const string IB = "IB";
    }
}
