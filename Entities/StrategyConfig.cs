﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WPExanteListenerOrders.Entities
{

    public class StrategyProfilesConfig
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public StrategyConfig[] StrategyProfiles { get; set; }
        public DateTime UpdatedTime { get; set; }
    }

    public class StrategyConfig
    {
        public string Name { get; set; }
        public RiskProfileEnum RiskProfile { get; set; }
        public decimal Lavarage { get; set; }
        public AssetClassConfig[] AssetClasses { get; set; }
    }

    public class AssetClassConfig
    {
        public string ClassName { get; set; }
        public AssetTypeConfig[] AssetTypes { get; set; }
    }

    public class AssetTypeConfig
    {
        public string Type { get; set; }
        public AssetTradeDataConfig[] ActiveIds { get; set; }
    }

    public class AssetTradeDataConfig
    {
        public string SymbolId { get; set; }
        public decimal Percent { get; set; }

        [BsonIgnoreIfNull]
        public string Type { get; set; }
        [BsonIgnoreIfDefault]
        public decimal Strike { get; set; }
        [BsonIgnoreIfDefault]
        public DateTime ExperationDate { get; set; }
        [BsonIgnoreIfDefault]
        public decimal Premium { get; set; } // if 0 then >>>
        [BsonIgnoreIfDefault]
        public int Count { get; set; }
        [BsonIgnoreIfNull]
        public string OrderType { get; set; }
        [BsonIgnoreIfDefault]
        public DateTime StartDate { get; set; }
        [BsonIgnoreIfDefault]
        public decimal StartLimitPrice { get; set; }
        [BsonIgnoreIfDefault]
        public decimal SellMultiplier { get; set; }
    }


    public static class AssetClasses
    {
        public const string Profitable = "Profitable";
        public const string Share = "Share";
        public const string Protective = "Protective";
        public const string GrowthStocks = "GrowthStocks";
        public const string Options = "Options";
        public const string IPO = "IPO";
        public const string FreeMoneyCover = "FreeMoneyCover";

    }

    public static class AssetTypes
    {
        public const string LongTermBonds = "LongTermBonds";
        public const string ShortTermBonds = "ShortTermBonds";
        public const string HighYieldCorporateBonds = "HighYieldCorporateBonds";
        public const string HighYieldGovBonds = "HighYieldGovBonds";
        public const string IndexFunds = "IndexFunds";
        public const string ActivelyManagedFunds = "ActivelyManagedFunds";
        public const string Cash = "Cash";
        public const string Gold = "Gold";
        public const string ReitFunds = "ReitFunds";
        public const string GrowthStocks = "GrowthStocks";
        public const string Options = "Options";
        public const string IPO = "IPO";
        public const string FreeMoneyCover = "FreeMoneyCover";
    }

    public static class OrderType
    {
        public const string Buy = "Buy";
        public const string Sell = "Sell";
    }

    public static class OptionType
    {
        public const string Put = "Put";
        public const string Call = "Call";
    }

    public static class StrategyType
    {
        public const string Strategy = "Strategy1";
        public const string MarketIPO = "MarketIPO";
    }
}
