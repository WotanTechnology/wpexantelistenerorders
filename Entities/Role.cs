﻿using System;
namespace WPExanteListenerOrders.Entities
{
    public static class Role
    {
        public const string Owner = "Owner";
        public const string Admin = "Admin";
        public const string User = "User";
        public const string Operator = "Operator";
    }
}
