using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WPExanteListenerOrders
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
            .UseKestrel(options =>
            {
                options.Limits.MaxConcurrentConnections = 100;
                options.Limits.MaxRequestBodySize = 1000 * 1024;
                options.Limits.MinRequestBodyDataRate =
                    new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(30));
                options.Limits.MinResponseDataRate =
                    new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(30));
                // options.Listen(IPAddress.Loopback, 5000);
            })
            .UseUrls("http://*:5006");
    }
}

//cd /Users/andreyvarnava/Repositories/WPExanteListenerOrders


//az login

//ЛОГИН к регистру контейнеров на ажуре
//az acr login --name WotanRegistry

// команды создания контейнера 
//docker build -t wotanregistry.azurecr.io/varnava/wp_exante_listener_orders .
//docker push wotanregistry.azurecr.io/varnava/wp_exante_listener_orders:latest



// Обновляем на ПРОДЕ
//kubectl set image deployment wp-exante-listener-orders wp-exante-listener-orders=wotanregistry.azurecr.io/varnava/wp_exante_listener_orders:latest --namespace ingress-basic  
//kubectl rollout restart deployment wp-exante-listener-orders --namespace ingress-basic

//kubectl apply -f azureWPExanteListenerOrdersModuleTest.yaml --namespace ingress-basic