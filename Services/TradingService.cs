﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WPExanteListenerOrders.Entities;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WPExanteListenerOrders.DTOs;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{
    public interface ITradingService
    {
        //  PortfolioData RebalancePortfolio(RequestTradingCommandDTO tradingData);

        Task<bool> SendProcessingTimeOutEvent(ProcessingTimeOutEvent newEvent);
    }

    public class TradingService : ITradingService
    {
        private readonly AppSettings _appSettings;

        private HttpClient _client;

        private IUserService _userService;


        public TradingService(HttpClient httpClient, IUserService userService, IDatabaseSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            _client = httpClient;

            _userService = userService;


            string connectionString = appSettings.Value.TradingContainerUrl;

            _client.BaseAddress = new Uri(connectionString);
        }

        public async Task SendAchievementTargetPriceEvent(AchievementTargetPriceEvent newEvent)
        {
            HttpResponseMessage response = PostAsync<AchievementTargetPriceEvent>("events/achievementTargetPrice", newEvent).Result;

            if (!response.IsSuccessStatusCode)
            {
                string error = response.Content.ReadAsStringAsync().Result;
                Error er = JsonConvert.DeserializeObject<Error>(error);

                throw new AppException(er.message, er.code);
            }
            await response.Content.ReadAsStringAsync();

            // var portfolioData = JsonConvert.DeserializeObject<PortfolioData>(await response.Content.ReadAsStringAsync());

            // return portfolioData;
        }

        public async Task<bool> SendProcessingTimeOutEvent(ProcessingTimeOutEvent newEvent)
        {
            HttpResponseMessage response = PostAsync<ProcessingTimeOutEvent>("events/processingTimeOut", newEvent).Result;

            if (!response.IsSuccessStatusCode)
            {
                string error = response.Content.ReadAsStringAsync().Result;
                Error er = JsonConvert.DeserializeObject<Error>(error);

                return false;
            }
            await response.Content.ReadAsStringAsync();

            return true;

            // var portfolioData = JsonConvert.DeserializeObject<PortfolioData>(await response.Content.ReadAsStringAsync());

            // return portfolioData;
        }

        //public PortfolioData RebalancePortfolio(RequestTradingCommandDTO tradingCommand)
        //{
        //    User user = _userService.GetUser(tradingCommand.UserEmail);

        //    StrategyData strategyData = _strategyService.GetStrategy((int)user.RiskProfileType);

        //    RequestTradingDataDTO requestTradingData = new RequestTradingDataDTO()
        //    {
        //        UserData = user,
        //        Strategy = strategyData,
        //        BrokerName = tradingCommand.BrokerName,
        //        Summ = tradingCommand.Summ,
        //        PlaceOrder = tradingCommand.PlaceOrder
        //    };

        //    HttpResponseMessage response = PostAsync<RequestTradingDataDTO>("trading/portfolio", requestTradingData).Result;

        //    if (!response.IsSuccessStatusCode)
        //    {
        //        string error = response.Content.ReadAsStringAsync().Result;
        //        Error er = JsonConvert.DeserializeObject<Error>(error);

        //        throw new AppException(er.message, er.code);
        //    }

        //    var result = response.Content.ReadAsStringAsync().Result;

        //    return  JsonConvert.DeserializeObject<PortfolioData>(result);
        //}

        private async Task<HttpResponseMessage> GetAsync(string endpoint)
        {

            try
            {
                var response = await _client.GetAsync($"{endpoint}");
                return response;
            }
            catch (TaskCanceledException ec)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (HttpRequestException eh)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (Exception ex)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }

        }

        private async Task<HttpResponseMessage> PostAsync<R>(string endpoint, R data)
        {
            try
            {
                var response = await _client.PostAsJsonAsync<R>($"{endpoint}", data);
                return response;
            }
            catch (TaskCanceledException ec)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (HttpRequestException eh)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (Exception ex)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }

        }
    }
}
