﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using WPExanteListenerOrders.DTOs;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{
    public class DetectProcessingTimeout : BackgroundService
    {
        private readonly IAccountService _accountService;
        private readonly ITradingService _tradingService;
        private readonly IUserService _userService;
        private readonly IMongoCollection<Symbol> _symbolsCollection;
        private readonly IMongoCollection<QuoteInfo> _quoteCollection;
        private readonly IMongoCollection<ProcessingTimeOutEvent> _processingTimeOutEventCollection;


        public DetectProcessingTimeout(IMongoDbService mongoDbService, ITradingService tradingService, IAccountService accountService, IUserService userService)
        {
            _accountService = accountService;
            _userService = userService;
            _tradingService = tradingService;

            _symbolsCollection = mongoDbService.GetSymbolIdsCollection();

            _quoteCollection = mongoDbService.GetExanteQuotesCollection();

            _processingTimeOutEventCollection = mongoDbService.GetProcessingTimeOutEventCollection();

     //       Console.WriteLine("ExanteQuotesService------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());

        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() => Console.WriteLine("ExanteOrdersService is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                Task.Run(async () =>
                {
                    await Listen().ConfigureAwait(false);
                }).ConfigureAwait(false);

                // End the background service
                break;
            }

            return Task.CompletedTask;
        }

        private async Task Listen()
        {

            while (true)
            {

                try
                {
                    //Console.WriteLine("Listen DetectProcessing-----------------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());
                    List<ProcessingTimeOutEvent> events = _processingTimeOutEventCollection.Find(t => t.Status == EventStatus.Placed).ToList();
                    await SendEventsInParallelInWithBatches(events).ConfigureAwait(false);

                }
                catch (AppException ex)
                {

                }

                Thread.Sleep(3000);
            }

        }

        private async Task<IEnumerable<bool>> SendEventsInParallelInWithBatches(List<ProcessingTimeOutEvent> events)
        {
            var tasks = new List<Task<bool>>();

            for (int i = 0; i < events.Count; i++)
            {
                try
                {
                    Task<bool> newTask = _tradingService.SendProcessingTimeOutEvent(events[i]);

                    //   await Task.Run(() => UpdateData(newTask.Result)).ConfigureAwait(false);

                    tasks.Add(newTask);

                    //Console.WriteLine("newTask " + TimeUtility.CurrentKyivTime());

                    if (newTask.Result)
                    {
                        
                        var update = Builders<ProcessingTimeOutEvent>.Update.Set("Status", EventStatus.Processed)
                            .Set("LastUpdatedUTCTime", DateTime.UtcNow);

                        _processingTimeOutEventCollection.UpdateOne(p => p.Id == events[i].Id, update);
                    }
                }
                catch (AppException ex)
                {
                    Console.WriteLine("E X E P T I O N " + TimeUtility.CurrentKyivTime());
                }

            }

          //  await Task.WhenAll(tasks);
            return (await Task.WhenAll(tasks));
        }

    }
}
