﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using WPExanteListenerOrders.DTOs;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{
    public class ExanteQuotesService : IHostedService
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;
        private readonly IMongoCollection<Symbol> _symbolsCollection;
        private readonly IMongoCollection<QuoteInfo> _quoteCollection;

        public ExanteQuotesService(IMongoDbService mongoDbService, IAccountService accountService, IUserService userService)
        {
            _accountService = accountService;
            _userService = userService;

            _symbolsCollection = mongoDbService.GetSymbolIdsCollection();

            _quoteCollection = mongoDbService.GetExanteQuotesCollection();

            //Console.WriteLine("ExanteQuotesService------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());

        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            BrokerInfo brokerInfo = _userService.GetUser("varnava.a@gmail.com").BrokerInfos[0];

            await Listen(brokerInfo).ConfigureAwait(false);

        }

        private async Task Listen(BrokerInfo brokerInfo)
        {

            while (true)
            {

                try
                {
                    //Console.WriteLine("Listen-----------------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());
                    List<Symbol> symbols = _symbolsCollection.Find(new BsonDocument()).ToList();

                    List<string> symbolsStr = new List<string>();

                    foreach (Symbol s in symbols)
                    {
                        symbolsStr.Add(s.SymbolId);
                    }

                    IEnumerable<QuoteInfo> quoteInfos = await GetQuotesInParallelInWithBatches(brokerInfo, symbolsStr.ToArray()).ConfigureAwait(false);
           
                }
                catch (AppException ex)
                {

                }

                Thread.Sleep(3000);
            }

        }

        

        public async Task<IEnumerable<QuoteInfo>> GetQuotesInParallelInWithBatches(BrokerInfo info, string[] symbols)
        {
            var tasks = new List<Task<IEnumerable<QuoteInfo>>>();
            var batchSize = 5;
            int numberOfBatches = (int)Math.Ceiling((double)symbols.Count() / batchSize);

            for (int i = 0; i < numberOfBatches; i++)
            {
                var currentIds = symbols.Skip(i * batchSize).Take(batchSize);

                Task<IEnumerable<QuoteInfo>> newTask = _accountService.GetLastQuote(info, currentIds.ToArray());

                await Task.Run(() => UpdateData(newTask.Result)).ConfigureAwait(false); 

                tasks.Add(newTask);

           }

            return (await Task.WhenAll(tasks)).SelectMany(u => u);
        }

        private void UpdateData(IEnumerable<QuoteInfo> quoteInfos)
        {
            
            foreach (QuoteInfo newData in quoteInfos)
            {
                QuoteInfo oldData = _quoteCollection.Find(t => t.symbolId == newData.symbolId).FirstOrDefault();

                if(oldData == null)
                {
                    _quoteCollection.InsertOne(newData);
                }
                else
                {
                    var update = Builders<QuoteInfo>.Update.Set("timestamp", newData.timestamp)
                        .Set("ask", newData.ask)
                        .Set("bid", newData.bid);

                    _quoteCollection.UpdateOne(t => t.symbolId == newData.symbolId, update);
                }
            }
            //Console.WriteLine("UpdateData------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {

            Console.WriteLine("StopAsync------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());
            // noop
            return Task.CompletedTask;
        }
    }
}
