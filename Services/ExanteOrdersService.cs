﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{
    public class ExanteOrdersService : BackgroundService
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;
        private readonly IMongoCollection<Symbol> _symbolsCollection;
        private readonly IMongoCollection<QuoteInfo> _quoteCollection;
        private readonly IMongoCollection<ResponceExanteOrder> _responceExanteOrder;
        private readonly IMongoCollection<PortfolioData> _portfolios;
        private readonly IMongoCollection<ProcessingTimeOutEvent> _processingTimeOutEventCollection;

        public ExanteOrdersService(IMongoDbService mongoDbService, IAccountService accountService, IUserService userService)
        {
            _accountService = accountService;
            _userService = userService;

            _symbolsCollection = mongoDbService.GetSymbolIdsCollection();

            _quoteCollection = mongoDbService.GetExanteQuotesCollection();

            _responceExanteOrder = mongoDbService.GetExanteOrdersCollection();

            _portfolios = mongoDbService.GetPortfoliosCollections();

            _processingTimeOutEventCollection = mongoDbService.GetProcessingTimeOutEventCollection();

          
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() => Console.WriteLine("ExanteOrdersService is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                Task.Run(async () =>
                {
                    await Listen().ConfigureAwait(false);
                }).ConfigureAwait(false);

                // End the background service
                break;
            }

            return Task.CompletedTask;
        }

        private async Task Listen()
        {

            while (true)
            {

                try
                {
                    //Console.WriteLine("Listen ExanteOrders-----------------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());
                    List<ResponceExanteOrder> orders = _responceExanteOrder.Find(t=>t.orderState.status != ExanteOrderStatus.Filled
                    && t.orderState.status != ExanteOrderStatus.Canceled
                    && t.orderState.status != ExanteOrderStatus.Rejected).ToList();

                    IEnumerable<ResponceExanteOrder> lastStatusOrders = await GetOrdersInParallelInWithBatches(orders).ConfigureAwait(false);

                }
                catch (AppException ex)
                {

                }

                Thread.Sleep(3000);
            }

        }

        private async Task<IEnumerable<ResponceExanteOrder>> GetOrdersInParallelInWithBatches(List<ResponceExanteOrder> orders)
        {
            var tasks = new List<Task<ResponceExanteOrder>>();

            for (int i = 0; i < orders.Count; i++)
            {
                if(orders[i].orderState.status == ExanteOrderStatus.BadRequest)
                {
                    await Task.Run(() => UpdateData(orders[i])).ConfigureAwait(false);
                    continue;
                }

                Task<ResponceExanteOrder> newTask = _accountService.GetOrder(orders[i]);

                if (newTask.Result != null)
                {
                    await Task.Run(() => UpdateData(newTask.Result)).ConfigureAwait(false);

                    tasks.Add(newTask);
                }

            }

            return (await Task.WhenAll(tasks));
        }

        private void UpdateData(ResponceExanteOrder newStatus)
        {

            if(newStatus == null)
            {
                return;
            }

            ResponceExanteOrder oldStatus = _responceExanteOrder.Find(t => t.id == newStatus.id).FirstOrDefault();

            if(oldStatus != null)
            {
                //Console.WriteLine("=========> " + newStatus.orderState.status);

                if(newStatus.orderState.status == ExanteOrderStatus.Filled || newStatus.orderState.status == ExanteOrderStatus.Canceled)
                {

                    ProcessingTimeOutEvent oldEvent = _processingTimeOutEventCollection.Find(t =>
                        t.OrderTag == newStatus.clientTag
                        ).FirstOrDefault();

                    if (oldEvent != null)
                    {
                        var updateEvent = Builders<ProcessingTimeOutEvent>.Update.Set("Status", EventStatus.Processed);

                        _processingTimeOutEventCollection.UpdateOne(p => p.Id == oldEvent.Id, updateEvent);
                    }
                }
                else
                {
                    
                    TimeSpan timeSpan1 = new TimeSpan(0, 15, 0);
                    TimeSpan timeSpanFromLastUpdate = DateTime.UtcNow - newStatus.orderState.lastUpdate;

                    if(newStatus.orderState.status != ExanteOrderStatus.Pending
                        && newStatus.orderParameters != null
                        && newStatus.orderParameters.orderType == ExanteOrderType.limit
                        &&  timeSpanFromLastUpdate > timeSpan1)
                    {
                        ProcessingTimeOutEvent oldEvent = _processingTimeOutEventCollection.Find(t =>
                        t.OrderTag == newStatus.clientTag 
                        ).FirstOrDefault();

                        if (oldEvent == null)
                        {
                            ProcessingTimeOutEvent newEvent = new ProcessingTimeOutEvent()
                            {
                                OrderTag = newStatus.clientTag,
                                Status = EventStatus.Placed,
                                OrderId = newStatus.id,
                                LastUpdatedUTCTime = DateTime.UtcNow
                            };

                            _processingTimeOutEventCollection.InsertOne(newEvent);
                        }
                        else
                        {               
                            TimeSpan timeSpanFromLastEventUpdate = DateTime.UtcNow - oldEvent.LastUpdatedUTCTime;
                            if (oldEvent.Status == EventStatus.Processed &&  timeSpanFromLastEventUpdate > timeSpan1)
                            {
                                var updateEvent = Builders<ProcessingTimeOutEvent>.Update.Set("Status", EventStatus.Placed)
                            .Set("LastUpdatedUTCTime", DateTime.UtcNow);

                                _processingTimeOutEventCollection.UpdateOne(p => p.OrderTag == newStatus.clientTag, updateEvent);
                            }
                        }
                    }
                }

                if(newStatus.orderState.status == ExanteOrderStatus.BadRequest)
                {
                    newStatus.orderState.status = ExanteOrderStatus.Rejected;
                }

                var update = Builders<ResponceExanteOrder>.Update.Set("orderState", newStatus.orderState);

                _responceExanteOrder.UpdateOne(t => t.id == newStatus.id, update);
            }

            string[] tags = newStatus.clientTag.Split("+");

            if(tags != null && tags.Length > 0)
            {
                PortfolioData portfolioData = _portfolios.Find(t => t.Id == tags[0]).FirstOrDefault();

                //Console.WriteLine("=========> " + newStatus.clientTag +" "+ tags[0]);

                if (portfolioData != null)
                {
                    List<OrderOperation> operations = portfolioData.OrderOperations.ToList();
                    OrderOperation foundedOrder = operations.FirstOrDefault(t => t.SymbolId == tags[1]);
                    if(foundedOrder != null)
                    {
                        operations.Remove(foundedOrder);
                        foundedOrder.Status = newStatus.orderState.status;
                        operations.Add(foundedOrder);
                    }

                    var update = Builders<PortfolioData>.Update.Set("OrderOperations", operations.ToArray());

                    _portfolios.UpdateOne(t => t.Id == tags[0], update);

                    if (newStatus.orderState.status == ExanteOrderStatus.Rejected)
                    {
                        Console.WriteLine("=========> REJECT " + newStatus.clientTag + " " + tags[0]);

                        List<Position> positions = portfolioData.TradePosition.ToList();
                        Position foundedPosition = positions.FirstOrDefault(t => t.SymbolId == tags[1]);
                        if (foundedPosition != null)
                        {
                            positions.Remove(foundedPosition);
                            foundedPosition.Quantity = foundedOrder.OperationType == OrderType.Buy ?
                                foundedPosition.Quantity - foundedOrder.Quantity :
                                foundedPosition.Quantity + foundedOrder.Quantity;
                            positions.Add(foundedPosition);

                           // update.Set("TradePosition", positions.ToArray());

                            var update2 = Builders<PortfolioData>.Update.Set("TradePosition", positions.ToArray());

                            _portfolios.UpdateOne(t => t.Id == tags[0], update2);
                        }
                    }

                    
                }
            }



            //Console.WriteLine("UpdateData------> " + TimeUtility.CurrentKyivTime().ToLongTimeString());
        }
    }
}
