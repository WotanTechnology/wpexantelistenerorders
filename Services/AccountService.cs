﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WPExanteListenerOrders.DTOs;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{
    public interface IAccountService
    {
        Account[] GetUserAccounts(BrokerInfo brokerInfo);
        AccountSummary GetAccountSummary(BrokerInfo brokerInfo);
        Task<IEnumerable<QuoteInfo>> GetLastQuote(BrokerInfo brokerInfo, string[] ids);
        Task<ResponceExanteOrder> GetOrder(ResponceExanteOrder exanteOrder);
    }

    public class AccountService : IAccountService
    {
        private readonly AppSettings _appSettings;

        private HttpClient _client;

        public AccountService(HttpClient httpClient, IDatabaseSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;

            _client = httpClient;

            string connectionString = appSettings.Value.APITradeEndPoint;
            _client.BaseAddress = new Uri(connectionString);
        }

        public AccountSummary GetAccountSummary(BrokerInfo brokerInfo)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}:{1}", brokerInfo.ApplicationID, brokerInfo.AccessKey));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            HttpResponseMessage response = GetAsync(string.Format("2.0/summary/{0}/USD", brokerInfo.AccountId)).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new AppException("", response.StatusCode);
            }

            var result = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<AccountSummary>(result);
        }

        public async Task<ResponceExanteOrder> GetOrder(ResponceExanteOrder exanteOrder)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}:{1}", exanteOrder.broker.ApplicationID, exanteOrder.broker.AccessKey));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            HttpResponseMessage response = GetAsync(string.Format("2.0/orders/{0}", exanteOrder.id)).Result;

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }


            var order = JsonConvert.DeserializeObject<ResponceExanteOrder>(await response.Content.ReadAsStringAsync());

            return order;
        }

        public async Task<IEnumerable<QuoteInfo>> GetLastQuote(BrokerInfo brokerInfo, string[] symbols)
        {

            var byteArray = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}:{1}", brokerInfo.ApplicationID, brokerInfo.AccessKey));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            string ids = string.Empty;

            for (int i = 0; i < symbols.Length; i++)
            {
                ids += symbols[i];
                
                if (i != symbols.Length - 1)
                {
                    ids += ",";
                }

            }

            HttpResponseMessage response = GetAsync(string.Format("2.0/feed/{0}/last", ids)).Result;

            if (!response.IsSuccessStatusCode)
            {
                string error = response.Content.ReadAsStringAsync().Result;
                Error er = JsonConvert.DeserializeObject<Error>(error);

                throw new AppException(er.message, er.code);
            }


            var users = JsonConvert.DeserializeObject<IEnumerable<QuoteInfo>>(await response.Content.ReadAsStringAsync());

            return users;
        }

        //public QuoteInfo[] GetLastQuote(RequestLastQuoteDTO requestLastQuoteDTO)
        //{
        //    var byteArray = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}:{1}", requestLastQuoteDTO.BrokerData.ApplicationID, requestLastQuoteDTO.BrokerData.AccessKey));
        //    _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

        //    List<string> quotesIds = new List<string>();

        //    string ids = string.Empty;


        //    for (int i = 0; i < requestLastQuoteDTO.SymbolsIds.Length; i++)
        //    {

        //        if (i % 5 == 0 && i != 0)
        //        {
        //            ids.Remove(ids.Length - 1, 1);
        //            quotesIds.Add(ids);
        //            ids = string.Empty;
        //            ids += requestLastQuoteDTO.SymbolsIds[i];
        //        }
        //        else
        //        {
        //            ids += requestLastQuoteDTO.SymbolsIds[i];
        //        }


        //        if (i != requestLastQuoteDTO.SymbolsIds.Length - 1 )
        //        {
        //            ids += ",";
        //        }

        //        if (i == requestLastQuoteDTO.SymbolsIds.Length - 1)
        //        {
        //            quotesIds.Add(ids);
        //        }
        //    }

        //    List<QuoteInfo> allQuotes = new List<QuoteInfo>();

        //    foreach (var idsStr in quotesIds)
        //    {
        //        HttpResponseMessage response = GetAsync(string.Format("2.0/feed/{0}/last", idsStr)).Result;

        //        if (!response.IsSuccessStatusCode)
        //        {
        //            string error = response.Content.ReadAsStringAsync().Result;
        //            Error er = JsonConvert.DeserializeObject<Error>(error);

        //            throw new AppException(er.message, er.code);
        //        }

        //        var result = response.Content.ReadAsStringAsync().Result;

        //        allQuotes.AddRange( JsonConvert.DeserializeObject<QuoteInfo[]>(result));
        //    }

        //    return allQuotes.ToArray();
        //}

        public Account[] GetUserAccounts(BrokerInfo brokerInfo)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}:{1}", brokerInfo.ApplicationID, brokerInfo.AccessKey));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));


            HttpResponseMessage response = GetAsync("2.0/accounts").Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new AppException("", response.StatusCode);
            }

            var result = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<Account[]>(result);
        }


        private async Task<HttpResponseMessage> GetAsync(string endpoint)
        {

            try
            {
                var response = await _client.GetAsync($"{endpoint}");
                return response;
            }
            catch (TaskCanceledException ec)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (HttpRequestException eh)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (Exception ex)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }

        }

        private async Task<HttpResponseMessage> PostAsync<R>(string endpoint, R data)
        {
            try
            {
                var response = await _client.PostAsJsonAsync<R>($"{endpoint}", data);
                return response;
            }
            catch (TaskCanceledException ec)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (HttpRequestException eh)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }
            catch (Exception ex)
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent("{\"message\":\"Internal Error\", \"code\":\"500\"}");
                return response;
            }

        }
    }
}
