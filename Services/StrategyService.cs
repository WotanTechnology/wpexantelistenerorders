﻿using System;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Bson;

namespace WPExanteListenerOrders.Services
{
    public interface IStrategyService
    {
        void UpdateStrategyConfig(StrategyConfig config);
        StrategyConfig GetStrategy(RiskProfileEnum riskProfileType);
        void UpdateTargetPrice(TargetPriceModel[] newPrices);
        TargetPriceModel[] GetTargetPrices(string symbolIds);
    }

    public class StrategyService : IStrategyService
    {
        private readonly AppSettings _appSettings;
        private readonly IMongoCollection<StrategyProfilesConfig> _strategyProfilesConfig;
        private readonly IMongoCollection<Symbol> _symbols;
        private readonly IMongoCollection<TargetPriceModel> _targetPrices;

        public StrategyService(IDatabaseSettings settings, IOptions<AppSettings> appSettings, IMongoDbService mongoDbService)
        {
            _appSettings = appSettings.Value;
            _strategyProfilesConfig = mongoDbService.GetStrategyConfigCollection();
            _symbols = mongoDbService.GetSymbolIdsCollection();
            _targetPrices = mongoDbService.GetTargetPricesCollection();
        }

        public StrategyConfig GetStrategy(RiskProfileEnum riskProfileType)
        {
            StrategyProfilesConfig strategyProfilesConfig = _strategyProfilesConfig.Find(new BsonDocument()).FirstOrDefault();

            if (strategyProfilesConfig == null || strategyProfilesConfig.StrategyProfiles == null)
            {
                throw new AppException("InvalidConfig", ErrorCodes.InvalidConfig);
            }

            StrategyConfig foundedConfig = strategyProfilesConfig.StrategyProfiles.FirstOrDefault(t=>t.RiskProfile == riskProfileType);

            if (foundedConfig == null)
            {
                throw new AppException("InvalidConfig", ErrorCodes.InvalidConfig);
            }

            return foundedConfig;
        }

        public void UpdateStrategyConfig(StrategyConfig newConfig)
        {
            if(newConfig.AssetClasses == null)
            {
                throw new AppException("InvalidConfig", ErrorCodes.InvalidConfig);
            }

            if (!CheckPercentages(newConfig))
            {
                throw new AppException("InvalidSummOfPercent", ErrorCodes.InvalidSummOfPercent);
            }


            RecognizeOptionId(newConfig);

            StrategyProfilesConfig strategyProfilesConfig = _strategyProfilesConfig.Find(new BsonDocument()).FirstOrDefault();

            bool newDocument = false;
            if(strategyProfilesConfig == null)
            {
                strategyProfilesConfig = new StrategyProfilesConfig();
                strategyProfilesConfig.StrategyProfiles = new StrategyConfig[0];
                newDocument = true;
            }

            List<StrategyConfig> strategyProfilesList = strategyProfilesConfig.StrategyProfiles.ToList();

            StrategyConfig previousConfig = strategyProfilesList.FirstOrDefault(s => s.RiskProfile == newConfig.RiskProfile);

            if(previousConfig != null)
            {
                strategyProfilesList.Remove(previousConfig);
            }

            strategyProfilesList.Add(newConfig);

            strategyProfilesConfig.StrategyProfiles = strategyProfilesList.ToArray();

            strategyProfilesConfig.UpdatedTime = TimeUtility.CurrentKyivTime();

            if (newDocument)
            {
                _strategyProfilesConfig.InsertOne(strategyProfilesConfig);
            }         
            else
            {
                var update = Builders<StrategyProfilesConfig>.Update.Set("StrategyProfiles", strategyProfilesConfig.StrategyProfiles)
                    .Set("UpdatedTime", strategyProfilesConfig.UpdatedTime);

                _strategyProfilesConfig.UpdateOne(new BsonDocument(), update);
            }


            foreach (AssetClassConfig eachClass in newConfig.AssetClasses)
            {
                foreach (AssetTypeConfig eachType in eachClass.AssetTypes)
                {
                    foreach (AssetTradeDataConfig eachTradeData in eachType.ActiveIds)
                    {
                        if(_symbols.Find(s=>s.SymbolId == eachTradeData.SymbolId).FirstOrDefault() == null)
                        {
                            Symbol newSymbol = new Symbol()
                            {
                                SymbolId = eachTradeData.SymbolId
                            };
                            _symbols.InsertOne(newSymbol);
                        }
                    }
                }
            }

        }

        private void RecognizeOptionId(StrategyConfig strategyData)
        {
            AssetClassConfig optionClass = strategyData.AssetClasses.FirstOrDefault(o => o.ClassName == AssetClasses.Options);

            if (optionClass == null)
            {
                return;
            }

            AssetTypeConfig optionType = optionClass.AssetTypes.FirstOrDefault(o => o.Type == AssetTypes.Options);

            if (optionType == null || optionType.ActiveIds == null || optionType.ActiveIds.Length == 0)
            {
                return;
            }

            foreach (AssetTradeDataConfig trade in optionType.ActiveIds)
            {
                trade.SymbolId = SetOptionSymbolId(trade);
            }
        }

        private string SetOptionSymbolId(AssetTradeDataConfig trade)
        {
            string dateString = string.Format("{0}{1}{2}",
                trade.ExperationDate.Day,
                OptionMounth.GetMounthSymbol(trade.ExperationDate.Month),
                trade.ExperationDate.Year);

            string side = trade.Type == OptionType.Call ? "C" : "P";
            side += trade.Strike.ToString().Replace(".", "_");

            string id = string.Format("{0}.{1}.{2}", trade.SymbolId, dateString, side);

            return id;
        }

        private bool CheckPercentages(StrategyConfig newConfig)
        {
            decimal summPercent = 0;
            foreach(AssetClassConfig eachClass in newConfig.AssetClasses)
            {
                foreach (AssetTypeConfig eachType in eachClass.AssetTypes)
                {
                    foreach (AssetTradeDataConfig eachTradeData in eachType.ActiveIds)
                    {
                        summPercent += eachTradeData.Percent;
                    }
                }
            }

            return summPercent <= 1 * newConfig.Lavarage;
        }

        public void UpdateTargetPrice(TargetPriceModel[] newPrices)
        {

            foreach (TargetPriceModel newPrice in newPrices)
            {
                TargetPriceModel oldPrice = _targetPrices.Find(t => t.SymbolId == newPrice.SymbolId).FirstOrDefault();

                if (oldPrice != null)
                {
                    var update = Builders<TargetPriceModel>.Update.Set("Price", newPrice.Price);

                    _targetPrices.UpdateOne(t=>t.SymbolId == newPrice.SymbolId, update);
                }
                else
                {
                    _targetPrices.InsertOne(newPrice);
                }
            }
        }

        public TargetPriceModel[] GetTargetPrices(string symbolIds)
        {
            string[] symbols = symbolIds.Split(',');

            List<TargetPriceModel> prices = new List<TargetPriceModel>();

            foreach(string s in symbols)
            {
                TargetPriceModel price = _targetPrices.Find(t => t.SymbolId == s).FirstOrDefault();

                if(price != null)
                {
                    prices.Add(price);
                }
            }

            return prices.ToArray();
        }
    }
}
