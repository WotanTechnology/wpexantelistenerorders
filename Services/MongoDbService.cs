﻿using System;
using WPExanteListenerOrders.Entities;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WPExanteListenerOrders.DTOs;
using WPExanteListenerOrders.Entities;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{
    public interface IMongoDbService
    {
        IMongoCollection<User> GetUserCollection();
        IMongoCollection<StrategyProfilesConfig> GetStrategyConfigCollection();
        IMongoCollection<Symbol> GetSymbolIdsCollection();
        IMongoCollection<TargetPriceModel> GetTargetPricesCollection();
        IMongoCollection<QuoteInfo> GetExanteQuotesCollection();
        IMongoCollection<ResponceExanteOrder> GetExanteOrdersCollection();
        IMongoCollection<PortfolioData> GetPortfoliosCollections();
        IMongoCollection<ProcessingTimeOutEvent> GetProcessingTimeOutEventCollection();
    }

    public class MongoDbService : IMongoDbService
    {
        private readonly IMongoCollection<User> _users;
        private readonly IMongoCollection<StrategyProfilesConfig> _strategyProfilesConfig;
        private readonly IMongoCollection<Symbol> _symbols;
        private readonly IMongoCollection<TargetPriceModel> _targetPrices;
        private readonly IMongoCollection<ResponceExanteOrder> _responceExanteOrder;
        private readonly IMongoCollection<PortfolioData> _portfolios;
        private readonly IMongoCollection<QuoteInfo> _qouteInfoCollection;
        private readonly IMongoCollection<ProcessingTimeOutEvent> _processingTimeOutEventCollection;

        public MongoDbService(IDatabaseSettings settings, IOptions<AppSettings> appSettings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _strategyProfilesConfig = database.GetCollection<StrategyProfilesConfig>(settings.StrategyConfigCollectionName);
            _symbols = database.GetCollection<Symbol>(settings.SymbolIdsCollectionName);
            _targetPrices = database.GetCollection<TargetPriceModel>(settings.TargetPriceCollectionName);

            _portfolios = database.GetCollection<PortfolioData>(settings.PortfoliosCollectionName);
            _qouteInfoCollection = database.GetCollection<QuoteInfo>(settings.ExanteQuotesCollectionName);
            _responceExanteOrder = database.GetCollection<ResponceExanteOrder>(settings.ExanteOrdersCollectionName);
            _processingTimeOutEventCollection = database.GetCollection<ProcessingTimeOutEvent>(settings.ProcessingTimeOutEventCollectionName);
        }

        public IMongoCollection<StrategyProfilesConfig> GetStrategyConfigCollection()
        {
            return _strategyProfilesConfig;
        }

        public IMongoCollection<User> GetUserCollection()
        {
            return _users;
        }

        public IMongoCollection<Symbol> GetSymbolIdsCollection()
        {
            return _symbols;
        }

        public IMongoCollection<TargetPriceModel> GetTargetPricesCollection()
        {
            return _targetPrices;
        }

        public IMongoCollection<QuoteInfo> GetExanteQuotesCollection()
        {
            return _qouteInfoCollection;
        }

        public IMongoCollection<ResponceExanteOrder> GetExanteOrdersCollection()
        {
            return _responceExanteOrder;
        }

        public IMongoCollection<PortfolioData> GetPortfoliosCollections()
        {
            return _portfolios;
        }

        public IMongoCollection<ProcessingTimeOutEvent> GetProcessingTimeOutEventCollection()
        {
            return _processingTimeOutEventCollection;
        }
    }
}
