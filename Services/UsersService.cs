﻿using System;
using System.Collections.Generic;
using System.Linq;
using WPExanteListenerOrders.Entities;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WPExanteListenerOrders.DTOs;
using WPExanteListenerOrders.Helpers;

namespace WPExanteListenerOrders.Services
{

    public interface IUserService
    {
        User GetUser(string id);
        User Registration(UserDTO registeredUser);
        void AddBrokerInfo(AddBrokerInfoRequest brokerInfo);
        User Authenticate(AuthenticateRequest authentication);
    }

    public class UsersService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IMongoCollection<User> _users;

        public UsersService(IDatabaseSettings settings, IOptions<AppSettings> appSettings, IMongoDbService mongoDbService )
        {
            _appSettings = appSettings.Value;
            _users = mongoDbService.GetUserCollection();
        }

        public void AddBrokerInfo(AddBrokerInfoRequest brokerInfoRequest)
        {
            User user = _users.Find<User>(t => t.Id == brokerInfoRequest.UserId).FirstOrDefault();

            if (user == null)
            {
                throw new AppException("UserNotFound", ErrorCodes.UserNotFound);
            }

            BrokerInfo brokerInfo = new BrokerInfo();
            brokerInfo.ApplicationID = brokerInfoRequest.ApplicationID;
            brokerInfo.BrokerName = brokerInfoRequest.BrokerName;
            brokerInfo.ClientId = brokerInfoRequest.ClientId;
            brokerInfo.SharedKey = brokerInfoRequest.SharedKey;
            brokerInfo.UserName = brokerInfoRequest.UserName;
            brokerInfo.Environment = brokerInfoRequest.Environment;

            List<BrokerInfo> brokersAccounts = new List<BrokerInfo>();
            if (user.BrokerInfos != null)
            {
                brokersAccounts = user.BrokerInfos.ToList();
            }

            BrokerInfo foundedBrokerInfo = brokersAccounts.FirstOrDefault(t => t.BrokerName == brokerInfo.BrokerName);

            if(foundedBrokerInfo != null)
            {
                brokersAccounts.Remove(foundedBrokerInfo);
            }

            brokersAccounts.Add(brokerInfo);

            var update = Builders<User>.Update.Set("BrokerInfos", brokersAccounts.ToArray());

            _users.UpdateOne(tr => tr.Id == brokerInfoRequest.UserId, update);
        }

        public User Authenticate(AuthenticateRequest authentication)
        {
            throw new NotImplementedException();
        }

        public User GetUser(string id)
        {
            User user = null;

            if (id.Contains("@"))
            {
                user = _users.Find<User>(t => t.Email == id).FirstOrDefault();
            }
            else
            {
                user = _users.Find<User>(t => t.Id == id).FirstOrDefault();
            }

            if (user == null)
            {
                throw new AppException("UserNotFound", ErrorCodes.UserNotFound);
            }

            return user;
        }

        public User Registration(UserDTO newUser)
        {
            
            if (string.IsNullOrWhiteSpace(newUser.Password))
                throw new AppException("InvalidUserPassword", ErrorCodes.InvalidUserPassword);

            if (_users.Find<User>(us => us.Email == newUser.Email).FirstOrDefault() != null)
                throw new AppException("EmailAlreadyTaken", ErrorCodes.EmailAlreadyTaken);

            if (!IsTelNumberOnly(newUser.Phone))
                throw new AppException("PhoneNumberNotValid", ErrorCodes.PhoneNumberNotValid);

            if (_users.Find<User>(us => us.Phone == newUser.Phone).FirstOrDefault() != null)
                throw new AppException("PhoneNumberAlreadyTaken", ErrorCodes.PhoneNumberAlreadyTaken);

            User user = new User();
            user.FirstName = newUser.FirstName;
            user.SecondName = newUser.SecondName;
            user.Email = newUser.Email;
            user.Phone = newUser.Phone;
            user.RiskProfileType = (RiskProfileEnum)newUser.RiskProfileType;

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(newUser.Password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            user.Role = Role.User;
            _users.InsertOne(user);

            return user;

        }

        bool IsTelNumberOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                {
                    if (c != '+')
                        return false;
                }
            }

            return true;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}
