﻿using System;
namespace WPExanteListenerOrders.Helpers
{
    public class OptionMounth
    {
        public static string GetMounthSymbol(int mounth)
        {
//            Январь – F
//Февраль – G
//Март – H
//Апрель – J
//Май – K
//Июнь – M
//Июль – N
//Август – Q
//Сентябрь – U
//Октябрь – V
//Ноябрь – X
//Декабрь – Z
            switch (mounth)
            {
                case 1:
                    return "F";
                case 2:
                    return "G";
                case 3:
                    return "H";
                case 4:
                    return "J";
                case 5:
                    return "K";
                case 6:
                    return "M";
                case 7:
                    return "N";
                case 8:
                    return "Q";
                case 9:
                    return "U";
                case 10:
                    return "V";
                case 11:
                    return "X";
                case 12:
                    return "Z";
            }

            return "";
        }
    }
}
