﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WPExanteListenerOrders.Helpers
{
    public class DatabaseSettings : IDatabaseSettings
    {
        public string StrategyConfigCollectionName { get; set; }
        public string UsersCollectionName { get; set; }
        public string RefreshTokensCollectionName { get; set; }
        public string SymbolIdsCollectionName { get; set; }
        public string TargetPriceCollectionName { get; set; }
        public string PortfoliosCollectionName { get; set; }
        public string ExanteQuotesCollectionName { get; set; }
        public string ExanteOrdersCollectionName { get; set; }
        public string ProcessingTimeOutEventCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDatabaseSettings
    {
        string StrategyConfigCollectionName { get; set; }
        string UsersCollectionName { get; set; }
        string RefreshTokensCollectionName { get; set; }
        string SymbolIdsCollectionName { get; set; }
        string TargetPriceCollectionName { get; set; }
        string ExanteQuotesCollectionName { get; set; }
        string ExanteOrdersCollectionName { get; set; }
        string ProcessingTimeOutEventCollectionName { get; set; }
        string PortfoliosCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
