﻿using System;
namespace WPExanteListenerOrders.Helpers
{
    public class TimeUtility
    {
        public static DateTime CurrentKyivTime()
        {
            TimeZoneInfo Eastern_Standard_Time = TimeZoneInfo.FindSystemTimeZoneById("Europe/Kiev");
            DateTime dateTime_Eastern = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Eastern_Standard_Time);
            return dateTime_Eastern;
        }
    }
}
