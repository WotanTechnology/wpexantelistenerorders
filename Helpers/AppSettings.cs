﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WPExanteListenerOrders.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string[] AdministrationsReceivers { get; set; }
        public string[] OwnerReceiver { get; set; }
        public TimeSpan TokenLifeTime { get; set; }

        public string BlobConnectionString { get; set; }
        public string BlobContainerName { get; set; }

        public string UsersContainerUrl { get; set; }
        public string TradingContainerUrl { get; set; }

        public string APIDataEndPoint { get; set; }
        public string APITradeEndPoint { get; set; }
    }

}
