﻿using System;
namespace WPExanteListenerOrders.DTOs
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Telegram { get; set; }

        public int RiskProfileType { get; set; }
        
    }

    public class AuthenticateRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class AddBrokerInfoRequest
    {
        public string UserId { get; set; }
        public string BrokerName { get; set; }
        public string UserName { get; set; }
        public string ClientId { get; set; }
        public string ApplicationID { get; set; }
        public string SharedKey { get; set; }
        public string Environment { get; set; }
        public string AccessKey { get; set; }
        public string AccountId { get; set; }
    }
}
