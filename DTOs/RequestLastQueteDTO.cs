﻿using System;
using WPExanteListenerOrders.Entities;

namespace WPExanteListenerOrders.DTOs
{
    public class RequestLastQuoteDTO
    {
        public BrokerInfo BrokerData { get; set; }
        public string[] SymbolsIds { get; set; }
    }
}
