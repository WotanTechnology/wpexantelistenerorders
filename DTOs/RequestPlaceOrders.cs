﻿using System;
using WPExanteListenerOrders.Entities;

namespace WPExanteListenerOrders.DTOs
{
    public class RequestPlaceOrdersDTO
    {
        public OrderOperation[] OrdersModel { get; set; }
        public BrokerInfo BrokerInfo { get; set; }
        public AccountSummary AccountInfo { get; set; }
    }
}
